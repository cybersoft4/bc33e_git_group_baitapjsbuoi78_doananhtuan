 
 let arrA=[]
 function handleSend(){
    
    const number=document.getElementById("number").value*1
    if(document.getElementById("number").value.trim()==""){
        return
    }else{
        arrA.push(number)
    }
    document.getElementById("number").value=""
    // lay the ele
    const listNumber=document.getElementById("listNumber")
    const soLuongSoDuong=document.querySelector(".soLuongSoDuong")
    const tongSoduong=document.querySelector(".tongSoduong")
    const soNhoNhat=document.querySelector(".soNhoNhat")
    const soDuongNhoNhat=document.querySelector(".soDuongNhoNhat")
    const soChanCuoiCung=document.querySelector(".soChanCuoiCung")
    const sapXep=document.querySelector(".sapXep")
    const soNguyenToDauTien1=document.querySelector(".soNguyenToDauTien")
    const soSanhSoLuongAmDuong1=document.querySelector(".soSanhSoLuongAmDuong1")
    // lay ket qua
    const ketqua1=tongcacsoduong(arrA)
    const ketqua2=cacsoduong(arrA)
    const ketqua3=sonhonhattrongmang(arrA)
    const ketqua4=soduongnhonhattrongmang(arrA)
    const ketqua5=sochancuoicung(arrA)
    const ketqua6=sapXep1(arrA)
    const ketqua7=soNguyenToDauTien(arrA)
    const ketqua8=soSanhSoLuongAmDuong(arrA)
    // xuat ra ket qua
    listNumber.innerHTML=arrA
    tongSoduong.innerHTML=ketqua1
    soLuongSoDuong.innerHTML=ketqua2
    soNhoNhat.innerHTML=ketqua3
    soDuongNhoNhat.innerHTML=ketqua4
    soChanCuoiCung.innerHTML=ketqua5
    sapXep.innerHTML=ketqua6
    soNguyenToDauTien1.innerHTML=ketqua7
    soSanhSoLuongAmDuong1.innerHTML=ketqua8
 }

//  hàm tính tổng các số dương
 function tongcacsoduong(arr){
    let sum=0
    const lengthArr=arr.length

    for(let i=0;i<lengthArr;i++){
        if(arr[i]>0){
            sum=sum+arr[i]
        }
    }
    return sum
 }

// hàm tính số lượng số dương
 function cacsoduong(arr){
    let k=0
    const lengthArr=arr.length

    for(let i=0;i<lengthArr;i++){
        if(arr[i]>0){
            k++
        }
    }
    return k
 }

// hàm tìm số nhỏ nhất trong mảng
 function sonhonhattrongmang(arr){
    let numMin=arr[0]
    const lengthArr=arr.length

    for(let i=0;i<lengthArr;i++){
        if(numMin>arr[i]){
            numMin=arr[i]
        }
    }
    return numMin
 }

// hàm tìm số dương nhỏ nhất trong mảng
 function soduongnhonhattrongmang(arr){
    const newArr=arr.filter(num=>num>0)
    
    let numMin=newArr[0]
    const lengthArr=newArr.length

    for(let i=0;i<lengthArr;i++){
        if(numMin>newArr[i]){
            numMin=newArr[i]
        }
    }
    return numMin
 }

//  hàm tìm số chẵn cuối cùng trong mảng
function sochancuoicung(arr){
    const soCuoi=arr[arr.length-1]
    if(soCuoi%2==0){
        return soCuoi
    }else return -1
}
// hàm đổi chỗ 2 giá trị trong mảng theo vị trí
function changeNumber(){
    const doiCho=document.querySelector(".doiCho")
    const index1=document.querySelector(".index1").value*1-1
    const index2=document.querySelector(".index2").value*1-1
    const newArr=[...arrA]
    newArr.splice(index1,0,newArr[index2])
    newArr.splice(index2+1,1)
    newArr.splice(index2+1,0,newArr[index1+1])
    newArr.splice(index1+1,1)
    doiCho.innerHTML=newArr
}

// Sắp xếp mảng theo thứ tự tăng dần.
function sapXep1(arr){
    const newArr=[...arr]
    newArr.sort((a, b)=>{return a - b})
    return newArr
}
//Tìm số nguyên tố đầu tiên trong mảng

function soNguyenTo(number){
    if(number<2){return false}
    if(number==2){return true}
    else if(number>2){
        for(let i=3;i<number-1;i+=2){
            if(number%i==0){
                return false
            }else{
                return true
            }
        }
    }else{
        return false
    }
}
const soNguyenToArr=[]
function soNguyenToDauTien(arr){
    for(let i=0;i<arr.length;i++){
        if(soNguyenTo(arr[i])){
            soNguyenToArr.push(arr[i])
        }
    }
    return soNguyenToArr[0]
}
// So sánh số lượng số dương và số lượng số âm
function soSanhSoLuongAmDuong(arr){
    let am=0
    let duong=0
    for(let i of arr){
        if(i>=0){
            duong++
        }else{am++}
    }
    if(duong>am){
        return "Số lượng số dương lớn hơn số âm"
    }else if(duong<am){
        return "Số lượng số âm lớn hơn số dương"
    }else{
        return "Số lượng số dương bằng số lượng số âm"
    }
}